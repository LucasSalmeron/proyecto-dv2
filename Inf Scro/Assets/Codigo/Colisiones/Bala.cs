﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

    [SerializeField]
    private float velocidad;
    [SerializeField]
    private float finalZ;
    [SerializeField]
    private float diferencia;
    [SerializeField]
    private bool enemigo;

    public void setEnemigo(bool p)
    {
        enemigo = p;
    }

    public void setFinal()
    {
        finalZ = this.transform.position.z + diferencia * Mathf.Sign(velocidad);
    }

    private void Update()
    {
        transform.Translate(0, 0, velocidad * Time.deltaTime);

       if(Mathf.Sign(velocidad) == 1)
        {
            if(this.transform.position.z > finalZ)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            if (this.transform.position.z < finalZ)
            {
                Destroy(this.gameObject);
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Pared"))
        {
            Destroy(other.gameObject);
            if (enemigo)
            {

            }else
            {
                ScoreManager.puntaje++;
            }
        }else
        {
            Destroy(this);
           
        }
    }





}

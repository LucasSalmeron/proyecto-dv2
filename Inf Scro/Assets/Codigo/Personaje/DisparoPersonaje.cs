﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoPersonaje : MonoBehaviour {


    [SerializeField]
    private GameObject prefabBala;
    [SerializeField]
    private float tiempoRecarga;
    [SerializeField]
    private bool recargado = true;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (recargado)
            {
                Disparar();
            }

        }
    }

    private void Recargar()
    {
        recargado = true;
    }

    private void Disparar()
    {
        GameObject bala = Instantiate(prefabBala);
        bala.transform.position = this.transform.position;
        bala.transform.rotation = this.transform.rotation;
        Bala scrBala = bala.GetComponent<Bala>();
        scrBala.setFinal();
        scrBala.setEnemigo(false);
        recargado = false;
        Invoke("Recargar", tiempoRecarga);
        
    }

   



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour {


    //Variables
    [SerializeField]
    private float speedX;
    [SerializeField]
    private Vector3 moveDirection;
    
   void Update()
     {
        float MovHorizontal = Input.GetAxis("Horizontal");
        moveDirection.x = MovHorizontal * speedX;
        transform.Translate(moveDirection*Time.deltaTime);
     }
}


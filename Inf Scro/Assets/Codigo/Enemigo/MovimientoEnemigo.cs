﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemigo: MonoBehaviour {


    [SerializeField]
    private Vector3 moveDirection;
    [SerializeField]
    private float tiempoCambio;
    [SerializeField]
    private float tiempo = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        tiempo = tiempo + Time.deltaTime;
        if(tiempo > tiempoCambio)
        {
            moveDirection.x = -moveDirection.x;
            tiempo = 0;
        }
        transform.Translate(moveDirection * Time.deltaTime);
	}
}

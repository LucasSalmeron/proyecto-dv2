﻿using UnityEngine;

public class Disparador : MonoBehaviour {

    [SerializeField] private GameObject prefabBala;
    [SerializeField] private float retrasoInicial;
    [SerializeField] private float cadenciaFuego;
    private void Awake()
    {
        Invoke("Disparar", retrasoInicial);
    }

    private void Disparar()
    {
        GameObject bala = Instantiate(prefabBala);
        bala.transform.position = this.transform.position;
        bala.transform.rotation = this.transform.rotation;
        Bala scrBala = bala.GetComponent<Bala>();
        scrBala.setFinal();
        scrBala.setEnemigo(true);
        Invoke("Disparar", cadenciaFuego);
    }

}
